import { AfterViewInit, Component, ElementRef, Inject, OnInit, Renderer2 } from '@angular/core';
import * as Highcharts from 'highcharts/highcharts-gantt';
import HC_exporting from 'highcharts/modules/exporting';
HC_exporting(Highcharts);
import HC_draggablePount from 'highcharts/modules/draggable-points';
import { DOCUMENT } from '@angular/common';
import { fromEvent } from 'rxjs';
HC_draggablePount(Highcharts);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  public highcharts: typeof Highcharts = Highcharts;
  public day = 1000 * 60 * 60 * 24;

  public chart: any;
  today = new Date() as any;

  constructor(@Inject(DOCUMENT) private document: Document, private elRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    console.log('document', this.document);
    console.log('rendere', this.renderer);
    this.renderer.setStyle(document.body, 'overflow', 'hidden');
    this.renderer.setStyle(document.body, 'margin', '0px');
    this.renderer.setStyle(document.body, 'height', '100%');
    // this.renderer.setStyle(document.body, 'width', '100%');
    this.createChart(window.innerHeight);
    fromEvent(window, 'resize')
      .pipe(
      // debounceTime(1000),
      // takeUntil(this._unsubscriber$)
    ).subscribe((evt: any) => {
      console.log('evt', evt);
      console.log('evt', evt.currentTarget.innerHeight);
      this.createChart(evt.currentTarget.innerHeight);
    })
  }

  ngAfterViewInit() { }

  createChart(height: number) {
    this.today.setUTCHours(0);
    this.today.setUTCMinutes(0);
    this.today.setUTCSeconds(0);
    this.today.setUTCMilliseconds(0);
    this.today = this.today.getTime();
    // this.chart = this.highcharts.ganttChart('container', {
    //   chart: {
    //     scrollablePlotArea: {
    //       // minHeight: 500
    //     }
    //   },
    //   title: {
    //     text: 'Gantt Chart with Progress Indicators'
    //   },
    //   xAxis: {
    //     min: Date.UTC(2014, 10, 17),
    //     max: Date.UTC(2014, 10, 30),
    //   },
    //   yAxis: {
    //     type: 'category',
    //     min: 0,
    //     max: 56,
    //     scrollbar: {
    //       enabled: true,
    //       showFull: false
    //     },
    //     categories: [
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests',
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests',
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests',
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests',
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests',
    //       'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests', 'Start prototype', 'Test prototype', 'Develop', 'Run acceptance tests'
    //     ],
    //   },
    //   series: [{
    //     type: 'gantt',
    //     dragDrop: {
    //       draggableX: true,
    //       draggableY: true,
    //       dragMinY: 0,
    //       dragMaxY: 56,
    //     },
    //     data: [{
    //       start: Date.UTC(2014, 10, 18),
    //       end: Date.UTC(2014, 10, 25),
    //       y: 0
    //     }, {
    //       start: Date.UTC(2014, 10, 27),
    //       end: Date.UTC(2014, 10, 29)
    //     }, {

    //       start: Date.UTC(2014, 10, 20),
    //       end: Date.UTC(2014, 10, 25),
    //       y: 1
    //     }, {

    //       start: Date.UTC(2014, 10, 23),
    //       end: Date.UTC(2014, 10, 26),
    //       y: 2
    //     }]
    //   }]
    // })
    let heigh = height;
    this.chart = Highcharts.ganttChart('container', {
      chart: {
        // height: 860,
        spacingLeft: 1,
        scrollablePlotArea: {
          minHeight: 60 * 24
        },
        marginBottom: 50
      },
      // scrollbar: {
      //   enabled: true
      // },
      title: {
        text: 'Interactive Gantt Chart'
      },
      subtitle: {
        text: 'Drag and drop points to edit'
      },
      lang: {
        accessibility: {
          axis: {
            xAxisDescriptionPlural: 'The chart has a two-part X axis showing time in both week numbers and days.'
          }
        }
      },
      // accessibility: {
      //   point: {
      //     descriptionFormatter: function (point) {
      //       return Highcharts.format(
      //         point.milestone ?
      //           '{point.name}, milestone for {point.yCategory} at {point.x:%Y-%m-%d}.' :
      //           '{point.name}, assigned to {point.yCategory} from {point.x:%Y-%m-%d} to {point.x2:%Y-%m-%d}.',
      //         { point }
      //       );
      //     }
      //   }
      // },
      plotOptions: {
        series: {
          animation: false, // Do not animate dependency connectors
          dragDrop: {
            draggableX: true,
            draggableY: true,
            dragMinY: 0,
            dragMaxY: 23,
            dragPrecisionX: this.day / 3 // Snap to eight hours
          },
          dataLabels: {
            enabled: true,
            format: '{point.name}',
            style: {
              cursor: 'default',
              pointerEvents: 'none'
            }
          },
          allowPointSelect: true,
          point: {
            events: {
              //     select: updateRemoveButtonStatus,
              //     unselect: updateRemoveButtonStatus,
              //     remove: updateRemoveButtonStatus
              drop: this.handleDropEvent
            }
          }
        }
      },

      yAxis: {
        type: 'category',
        categories: [
          'Tech', 'Marketing', 'Sales', 'Tech1', 'Marketing1', 'Sales1',
          'Tech1', 'Marketing1', 'Sales1', 'Tech1', 'Marketing1', 'Sales1',
          'Tech2', 'Marketing2', 'Sales2', 'Tech2', 'Marketing2', 'Sales2',
          'Tech3', 'Marketing3', 'Sales3', 'Tech3', 'Marketing3', 'Sales3',
        ],
        accessibility: {
          description: 'Organization departments'
        },
        min: 0,
        max: 23,
        // scrollbar: {
          // enabled: true,
          // showFull: true
        // }
      },
      xAxis: {
        // scrollbar: {
        //   enabled: true
        // },
        currentDateIndicator: true,
      },
      tooltip: {
        xDateFormat: '%a %b %d, %H:%M'
      },

      series: [{
        name: 'Project 1',
        type: 'gantt',
        data: [{
          start: this.today + 2 * this.day,
          end: this.today + this.day * 5,
          name: 'Prototype',
          id: 'prototype',
          y: 0
        }, {
          start: this.today + this.day * 6,
          name: 'Prototype done',
          // milestone: true,
          dependency: 'prototype',
          id: 'proto_done',
          y: 0
        }, {
          start: this.today + this.day * 7,
          end: this.today + this.day * 11,
          name: 'Testing',
          dependency: 'proto_done',
          y: 0
        }, {
          start: this.today + this.day * 5,
          end: this.today + this.day * 8,
          name: 'Product pages',
          y: 1
        }, {
          start: this.today + this.day * 9,
          end: this.today + this.day * 10,
          name: 'Newsletter',
          y: 1
        }, {
          start: this.today + this.day * 9,
          end: this.today + this.day * 11,
          name: 'Licensing',
          id: 'testing',
          y: 2
        }, {
          start: this.today + this.day * 11.5,
          end: this.today + this.day * 12.5,
          name: 'Publish',
          // dependency: 'testing',
          y: 2
          },
          {
            start: this.today + this.day * 11.5,
            end: this.today + this.day * 12.5,
            name: 'Publish',
            // dependency: 'testing',
            y: 3
          },
          {
            start: this.today + this.day * 11.5,
            end: this.today + this.day * 12.5,
            name: 'Publish',
            // dependency: 'testing',
            y: 10
          }
        ]
      }]
    });
  }


  public handleDropEvent = (data: any) => {
    console.log('dropevent', data.target.options);
  }

}
